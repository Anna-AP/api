
# **TEST API**
_Test application as part of a job candidacy._

-----------------

#### **STUDENT**

* **Get collection Student**
  * **URL**: /api/students
  * **Method:** `GET`
  * **Success Response:** 200
  * **Schema Response:** 
        `{
            "id": number, 
            "name": "string", 
            "firstname": "string", 
            "birthday": DateTime, 
            "grades": [
            	"id": number,
                "grade": number,
                "subject": "string",
            ]
        }`
      
* **Get Student**
  * **URL**: /api/students/{id}
  * **Method:** `GET`
  * **Required:** `id=[number]`
  * **Success Response:** 200
  * **Schema Response:** 
         `{
            "id": number, 
            "name": "string", 
            "firstname": "string", 
            "birthday": DateTime, 
            "grades": [
            	"id": number,
                "grade": number,
                "subject": "string"
            ]
        }`
        
* **Add Student**
  * **URL**: /api/students
  * **Method:** `POST`
  * **Data Params Required:** 
        `{
            "name": [string], 
            "firstname": [string]
        }`
  * **Data Params Optional:**
 		`{
            "birthday": [Datetime], 
        }`
  * **Success Response:** 201
  * **Schema Response:** 
        `{
            "id": number, 
            "name": "string", 
            "firstname": "string", 
            "birthday": DateTime, 
            "grades": []
        }`

* **Update Student**
  * **URL**: /api/students/{id}
  * **Method:** `PUT`
  * **Required:** `id=[number]`
  * **Success Response:** 200
  * **Schema Response:** 
         `{
            "id": number, 
            "name": "string", 
            "firstname": "string", 
            "birthday": DateTime, 
            "grades": [
            	"id": number,
                "grade": number,
                "subject": "string"
            ]
        }`
        
* **Delete Student**
  * **URL**: /api/students/{id}
  * **Method:** `DELETE`
  * **Required:** `id=[number]`
  * **Success Response:** 204

* **Get average Student**
  * **URL**: /api/student/{id}/average
  * **Method:** `GET`
  * **Required:** `id=[number]`
  * **Success Response:** 200
  * **Schema Response:** 
         `{
            "average": number
        }`
 
-----------------
 #### **GRADES**
        
* **Get collection Grades**
  * **URL**: /api/grades
  * **Method:** `GET`
  * **Success Response:** 200
  * **Schema Response:** 
        `{
            "id": number,
            "grade": number,
            "subject": "string"
        }`   
        
* **Add Grades**
  * **URL**: /api/grades
  * **Method:** `POST`
  * **Data Params Required:** 
        `{
            "grade": [number, [0-20]], 
            "subject": [string],
            "student": "api/students/{id}"
        }`
  * **Success Response:** 201
  * **Schema Response:** 
        `{
            "id": number,
            "grade": number,
            "subject": "string"
        }` 

* **Get average Grades**
  * **URL**: /api/grades/average
  * **Method:** `GET`
  * **Success Response:** 200
  * **Schema Response:** 
         `{
            "average": number
        }`
