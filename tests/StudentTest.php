<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Student;

class StudentTest extends ApiTestCase
{
    public function testGetCollectionStudent(): void
    {
        $response = static::createClient()
            ->request('GET', '/api/students');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(Student::class);
    }

    public function testCreateStudent(): void
    {
        $response = static::createClient()
            ->request('POST', '/api/students', [
                'json' => [
                    'name' => 'NOM',
                    'firstname' => 'Firstname',
                    'birthday' => '1990-01-10T12:01:04.590Z',
                ]
            ]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertResponseStatusCodeSame(201);
        $this->assertMatchesResourceItemJsonSchema(Student::class);
    }

    public function testUpdateStudent(): void
    {
        $client = static::createClient();
        $iri = $this->findIriBy(Student::class, ['name' => 'NOM']);
        $client->request('PUT', $iri, [
            'json' => [
                'name' => 'NOM 2',
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceItemJsonSchema(Student::class);
    }

    public function testDeleteStudent(): void
    {
        $client = static::createClient();
        $iri = $this->findIriBy(Student::class, [
            'name' => 'NOM 2'
        ]);

        $client->request('DELETE', $iri);

        $this->assertResponseStatusCodeSame(204);
        $this->assertNull(
            static::$container->get('doctrine')->getRepository(Student::class)->findOneBy(['name' => 'NOM 2'])
        );
    }

    public function testGetAverageStudent(): void
    {
        $response = static::createClient()
            ->request('GET', '/api/student/3/average');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertResponseStatusCodeSame(200);
    }
}