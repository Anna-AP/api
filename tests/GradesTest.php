<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Grades;

class GradesTest extends ApiTestCase
{
    public function testGetCollectionGrades(): void
    {
        $response = static::createClient()
            ->request('GET', '/api/grades');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertResponseStatusCodeSame(200);
        $this->assertMatchesResourceCollectionJsonSchema(Grades::class);
    }

    public function testCreateGrades(): void
    {
        $response = static::createClient()
            ->request('POST', '/api/grades', [
                'json' => [
                    'grade' => 15,
                    'subject' => 'Matière A',
                    'student' => 'api/students/3',
                ]
            ]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertResponseStatusCodeSame(201);
        $this->assertMatchesResourceItemJsonSchema(Grades::class);
    }

    public function testGetAverageGrades(): void
    {
        $response = static::createClient()
            ->request('GET', '/api/grades/average');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/json');
        $this->assertResponseStatusCodeSame(200);
    }

    public function testCreateInvalidGrades(): void
    {
        static::createClient()
            ->request('POST', '/api/grades', [
                'json' => [
                    'grade' => 21,
                    'subject' => 'Matière A',
                    'student' => 'api/students/3',
                ]
            ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }
}