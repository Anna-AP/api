<?php

namespace App\Controller\Api;

use App\Service\GradesService;
use Symfony\Component\HttpFoundation\JsonResponse;


class GradesAverageAllScore
{
    private $gradesService;

    public function __construct(GradesService $gradesService)
    {
        $this->gradesService = $gradesService;
    }

    public function __invoke()
    {
        $average = $this->gradesService->getAverageAll();

        $response = new JsonResponse(['average_class' => $average]);

        return $response;
    }
}