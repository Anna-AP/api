<?php

namespace App\Controller\Api;

use App\Entity\Student;
use App\Service\StudentService;
use Symfony\Component\HttpFoundation\JsonResponse;


class StudentAverageScore
{
    private $studentsService;

    public function __construct(StudentService $studentsService)
    {
        $this->studentsService = $studentsService;
    }

    public function __invoke(Student $data)
    {
        if(count($data->getGrades()->toArray()) > 0) {
            $average = $this->studentsService->getAverage($data->getGrades()->toArray());
        } else {
            $average = null;
        }

        $response = new JsonResponse(['average' => $average]);

        return $response;
    }
}