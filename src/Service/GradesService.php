<?php

namespace App\Service;

use App\Entity\Grades;
use Doctrine\ORM\EntityManagerInterface;

class GradesService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getAverageAll()
    {
        $grades = $this->em->getRepository(Grades::class)->findAll();
        $all_grades = 0;

        if(count($grades) > 0) {
            foreach ($grades as $i => $grade) {
                $all_grades += $grade->getGrade();
            }
            $average = round($all_grades / count($grades), 1, PHP_ROUND_HALF_EVEN);
        } else {
            $average = null;
        }

        return $average;
    }
}