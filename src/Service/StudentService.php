<?php

namespace App\Service;

use App\Entity\Student;
use Doctrine\ORM\EntityManagerInterface;

class StudentService
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getAverage($grades)
    {
        $all_grades = 0;

        foreach ($grades as $i => $grade) {
            $all_grades += $grade->getGrade();
        }

        $average = round($all_grades / count($grades), 1, PHP_ROUND_HALF_EVEN);

        return $average;
    }
}