<?php

namespace App\Entity;

use App\Repository\GradesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ApiResource(
 *     normalizationContext={
 *          "groups"={"read:full:student"}
 *     },
 *     collectionOperations={
 *          "get",
 *          "get_average_class"={
 *              "method"="GET",
 *              "path"="/grades/average",
 *              "controller"=App\Controller\Api\GradesAverageAllScore::class,
 *          },
 *          "post",
 *     },
 *     itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"read:full:student"}}
 *          },
 *     },
 * )
 * @ORM\Entity(repositoryClass=GradesRepository::class)
 */
class Grades
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read:full:student"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *     min=0,
     *     max=20,
     *     notInRangeMessage="Grade must be between 0 and 20."
     * )
     * @Groups({"read:full:student"})
     */
    private $grade;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read:full:student"})
     */
    private $subject;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="grades")
     */
    private $student;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGrade(): ?int
    {
        return $this->grade;
    }

    public function setGrade(int $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getStudent(): Student
    {
        return $this->student;
    }

    public function setStudent(Student $student): self
    {
        $this->student = $student;

        return $this;
    }
}
